import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { ApplicationRef, NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TopBarComponent } from "./top-bar/top-bar.component";
import { PageComponent } from "./page/page.component";
import { ModifyPageComponent } from "./page/modify-page/Modify-page.component";
import { PageListComponent } from "./page-list/page-list.component";
import { CommonModule } from "@angular/common";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginComponent } from './login/login.component';
//const obj:any={};
const routes: Routes = [
  { path: "page-list.component", component: PageListComponent },
  { path: "page.component/:id2", component: PageComponent},
  { path: "Modify-page.component", component: ModifyPageComponent },
  { path: '', component: LoginComponent },

  
];

@NgModule({
  imports: [BrowserModule,   AppRoutingModule, HttpClientModule,ReactiveFormsModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [
    AppComponent,
    PageListComponent,
    PageComponent,
    TopBarComponent,
    ModifyPageComponent,
    LoginComponent
  ],
  providers: [ TopBarComponent],
  bootstrap: [AppComponent]
})
export class AppModule { 
  public ngDoBootstrap(appRef: ApplicationRef) {
    try {
      appRef.bootstrap(AppComponent);
    } catch (e) {
      console.log("Application element not found");
    }
  }
}
