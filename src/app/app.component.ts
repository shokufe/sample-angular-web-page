import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(){}

  gotoPortal(){
    document.location.href='https://localhost:5001/portal/';
  }
  goToCreate(){
    document.location.href='https://localhost:5001/portal/page/create';
  }
  title = 'angular-httpclient';
  
}
