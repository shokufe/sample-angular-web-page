import { Component, OnInit } from "@angular/core";
import { ApiService } from '.././api.service';
import { Router } from "@angular/router";
import {PageComponent} from "../page/page.component";
import { HttpClient ,HttpHeaders,HttpParams,HttpResponse} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { stringify } from "@angular/compiler/src/util";
import { Injectable, Type } from '@angular/core';

@Component({
  selector: "app-product-list",
  templateUrl: "./page-list.component.html",
  styleUrls: ["./page-list.component.css"]
})
export class PageListComponent {
  message;  
  role;
  user;
  constructor(private api: ApiService ,private router: Router,private http: HttpClient,route:ActivatedRoute) {
       
    
      this.message=''
      if(localStorage.getItem('token')==''){
        alert('you should first login')
        this.router.navigate(['']);
      }
       this.user=localStorage.getItem('user')
       this.role=localStorage.getItem('role')
      if (this.role== "SuperAdmin"){
        this.testSource()
      }else{
        this.testSourceLimite(this.user)
      }
  

      }
      
 
      goToPage(id2:number){
      //  alert(id2)
        this.router.navigate(['page.component', id2]);
      }
      goToCreate(){
        document.location.href='https://localhost:5001/portal/page/create';
      }
      m:any;
      pages: any = [];
      testSource(){
        var v= localStorage.getItem('token')
       
        var reqHeader = new HttpHeaders({ 
          'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
        });
        this.http.get('/api/v1/rest/en-us/page/portal', { headers: reqHeader }).subscribe(data => {
          var datad=JSON.parse(JSON.stringify(data)) 
          var dataItems=datad.items
          for (const d of (dataItems as any)) {
            this.pages.push({
              id:d.id,
              img: d.image,
              content:d.content,
              title: d.title,
              createdBy:d.createdBy,
              Langue: d.specificulture ,
              createdDateTime: d.createdDateTime,
              child:'d.child',
              detailsUrl:'d.detailsUrl',
              isActived:'d.isActived',
              
              template:'d.template'
            });
          }
          
          //this.m=data
         // this.message=stringify(data)
          })
 
      }
   // pages: any = [];
      getPages() {
        this.api.getallPages()
          .subscribe(data => {
            for (const d of (data as any)) {
              this.pages.push({
                id:d.id,
                content:d.content,
                title: d.title,
                createdBy:'d.createdBy',
                child:'d.child',
                detailsUrl:'d.detailsUrl',
                isActived:'d.isActived',
                createdDateTime:'d.createdDateTime',
                template:'d.template'
              });
            }
            console.log(this.pages);
          });
      }
    
      share() {
        window.alert("shared!");
      }
      onNotify() {
        window.alert("Notify");
      }
     
      
      Modify(){
        window.alert("Modify!");
      }
      testSourceLimite(user3:any){
       
       
        var reqHeader = new HttpHeaders({ 
          'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
        });
        this.http.get('/api/v1/rest/en-us/page/portal', { headers: reqHeader }).subscribe(data => {
          var datad=JSON.parse(JSON.stringify(data)) 
          var dataItems=datad.items
          for (const d of (dataItems as any)) {
            if(d.createdBy == user3){
              this.pages.push({
                id:d.id,
                img: d.image,
                content:d.content,
                title: d.title,
                createdBy:d.createdBy,
                Langue: d.specificulture ,
                createdDateTime: d.createdDateTime,
                child:'d.child',
                detailsUrl:'d.detailsUrl',
                isActived:'d.isActived',
                
                template:'d.template'
              });
            }
           
          }
          
          //this.m=data
         // this.message=stringify(data)
          })
      }
}


