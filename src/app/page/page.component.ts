/**
 * page componenet
 */
import { Component} from "@angular/core";
import { ApiService } from '.././api.service';
import {ModifyApiService} from '.././modifyApi.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { HttpClient ,HttpHeaders,HttpParams,HttpResponse} from '@angular/common/http';

@Component({
  selector: "app-product-list",
  templateUrl: "./page.component.html",
  styleUrls: ["./page.component.css" ]
})

export class PageComponent   {
  constructor( private api: ApiService,private router: Router ,route:ActivatedRoute,private http: HttpClient,) {   
   const id: number = route.snapshot.params.id2;
   var id2 :number;
   var title:string;
   this.getAll(id);
   this.getInfoDePage(id);
   var user= localStorage.getItem('user')
   console.log("user is "+user)

  }

  page: any;
  title: any;
  content :any;
  image:any;
  createDate:any;
  createBy:any;
  id2:any;
  userPer:any;
  styles:any;
  getAll(cible:number){
    this.api.getallPages()
    .subscribe(data => {
      var datad=JSON.parse(JSON.stringify(data))
      var dataItems=datad.items
      for (const d of (dataItems as any)) {       
        if (d.id == cible){
          this.id2=d.id;
          this.title=d.title;
        //  this.content=d.content;
          this.image=d.image;
          this.createDate=d.createdDateTime;
          this.createBy=d.createdBy;
          this.userPer=localStorage.getItem('user');
        }
     
      }
      console.log(this.page);
    });
   }

   getInfoDePage(cible:number){
   
    var v= localStorage.getItem('token')

    var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
     this.http.get('/api/v1/rest/en-us/mix-page/mvc/'+cible).subscribe(data => {

      var datac=JSON.parse(JSON.stringify(data))
      this.content=datac.content
      this.styles=datac.view.styles
      console.log(this.styles)
   
     }
      
      )


   }
   convertToHtML(str:string){
    var dom = document.createElement('div');
    dom.innerHTML = str;
    return dom;
   }


   ModifyPage () {
     var url = this.api.sendModifyReq(this.id2).subscribe();
     console.log(this.id2)
     document.location.href='https://localhost:5001/portal/page/details/'+this.id2;
  };

  logout(){
      alert("I want to logout")
      localStorage.setItem('token','');
      this.router.navigate(['']);
  }
}


