import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders,HttpParams,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};
//const localUrl = 'assets/data/pages1.json'; // we put url
const localUrl='/api/page';
const authUrl='/api/token';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  modifyUrl = 'http://127.0.0.1:5000/portal/page/details';// URL to web api
  constructor(private http: HttpClient) { }

  getallPages(){
  //  alert(this.http.get(localUrl))
    //console.log(this.http.get(localUrl))
    var v= localStorage.getItem('token')

    var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    return this.http.get('/api/v1/rest/en-us/page/portal', { headers: reqHeader })



}
  doLogin():Observable<boolean>{
   // const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    var headers = new HttpHeaders();
headers.append('Content-Type', 'application/x-www-form-urlencoded');
headers.append('Accept', 'application/json');
    const body = new HttpParams()
    .set('username', 'Anurag')
    .set('password', '123456')
    .set('grant_type', 'password');
    return this.http.post('/api/token', body.toString(),{ headers, observe: 'response' }).map(
      (res: HttpResponse<Object>) =>
        res.ok,
        console.log('hello')
       )
      .catch((err: any) => Observable.of(false));
     
  }

  sendModifyReq(id: number): Observable<{}> {
    const url = `${this.modifyUrl}/${id}`;  
   // alert(url); 
    return this.http.post<{}>(url, httpOptions) //api/modify/4
      .pipe(
       // catchError(this.handleError('modifyPage', id))
      );
  }
}
