import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PageComponent } from './page/page.component';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';

const localUrl = 'assets/data/pages1.json'; // we put url
//const localUrl='http://127.0.0.1:5000/api/v1/rest/fr-fr/page/portal';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ModifyApiService {
//  heroesUrl = 'api/heroes';  
   modifyUrl = 'api/modify';// URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('ModifyApiService');
  }


  sendModifyReq(id: number) {
    const url = `${this.modifyUrl}/${id}`;  
    return url
    /*return this.http.post<{}>(url, httpOptions) //api/modify/4
      .pipe(
        catchError(this.handleError('modifyPage', id))
      );*/
  }


/*{
    myAppUrl: any;
    myApiUrl: any;
    httpOptions = {
        headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8'
        })
    };
  constructor(private http: HttpClient) { 
   // this.myAppUrl = environment.appUrl;
    this.myApiUrl = 'api/BlogPosts/';
  }*/
 
  
}
