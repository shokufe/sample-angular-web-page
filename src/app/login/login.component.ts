import { Component, OnInit } from '@angular/core';
import { ApiService } from '.././api.service';
import { Router } from "@angular/router";
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClient ,HttpHeaders,HttpParams,HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
formdata;
token2:any;
loginstatus;
show;
  constructor(private router: Router,private http: HttpClient,private api: ApiService) {
       // this.getPages();
   // this.getPages();
    this.loginstatus=false;
    this.show="";
    this.formdata = new FormGroup({
    username: new FormControl(""),
    password: new FormControl("")
 });

   }

  ngOnInit(): void {
  }


  onClickSubmit(data:any)  {
    var token=null;
    var datapart=null;
    var user=data.username;
    
    var accessToken;
    var time;
    console.log(data.username + data.password)

    const body = new HttpParams()
    .set('username', data.username)
    .set('password', data.password)
    .set('grant_type', 'password');
   
    this.http.post('/api/v1/account/login', { username: data.username , password: data.password }).subscribe(data => {
      console.log(data)
      token=JSON.parse(JSON.stringify(data))
      datapart=token.data
      accessToken =datapart.access_token
      var getrole= datapart.userData
     
      localStorage.setItem('role',getrole.userRoles[0].role.name)
      localStorage.setItem('token',accessToken)
    //  var role=token.userData.userRoles
   // alert(datapart.userData.userRoles.role.name)
    
      this.loginstatus=true
      this.show = "Login Successful"
      if (token !=null){
        localStorage.setItem('user',user)
        this.router.navigate(['page-list.component']);
      }else{
        alert("Something wrong")
      }
      
  })


   /* var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    const body = new HttpParams()
    .set('username', data.username)
    .set('password', data.password)
    .set('grant_type', 'password');
   
    this.http.post('/api/token', body.toString(),{ headers }).subscribe(data => {
    //  console.log(typeof(JSON.stringify(data) ))
     token=JSON.parse(JSON.stringify(data))
     accessToken=token.access_token
     localStorage.setItem('token',accessToken)
    
     this.loginstatus=true
     this.show = "Login Successful"
   //  var value= localStorage.getItem('token')
    if (token !=null){

      localStorage.setItem('user',user)
    }
     this.router.navigate(['page-list.component']);
  })

  */


  }




}